terraform {
  required_providers {
    kubernetes = "~> 2.3"
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.0"
    }
    graphql = {
      source = "sullivtr/graphql"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~>3.6.0"
    }
  }
}
